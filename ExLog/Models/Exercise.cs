using System.ComponentModel.DataAnnotations;

namespace ExLog.Models;

public class Exercise
{
    public int Id { get; set; }
    
    [Required]
    [MaxLength(50)]
    public string Name { get; set; }
    
    public int NumberOfSets { get; set; }
    public int NumberOfReps { get; set; }
    public int Weight { get; set; }
}