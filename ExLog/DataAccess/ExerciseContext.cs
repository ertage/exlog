using ExLog.Models;
using Microsoft.EntityFrameworkCore;

namespace ExLog.DataAccess;

public class ExerciseContext : DbContext
{ 
   public ExerciseContext(DbContextOptions options) : base(options) {}
   
   public DbSet<Exercise> Exercises { get; set; }
}