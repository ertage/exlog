using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using ExLog.DataAccess;
using ExLog.Models;

namespace ExLog.Pages.Exercises
{
    public class IndexModel : PageModel
    {
        private readonly ExLog.DataAccess.ExerciseContext _context;

        public IndexModel(ExLog.DataAccess.ExerciseContext context)
        {
            _context = context;
        }

        public IList<Exercise> Exercise { get;set; } = default!;

        public async Task OnGetAsync()
        {
            if (_context.Exercises != null)
            {
                Exercise = await _context.Exercises.ToListAsync();
            }
        }
    }
}
